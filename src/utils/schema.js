import * as yup from 'yup';


export const signInSchema = yup.object().shape({
    email: yup.string().min(6).max(40).required('Required field').email('Email bad formatted'),
    password: yup.string().min(6).max(40).required('Required Field')
});

export const signUpSchema = yup.object().shape({
    name: yup.string().required().min(4).max(35),
    email: yup.string().min(6).max(40).required('Required field').email('Email bad formatted'),
    password: yup.string().min(6).max(40).required('Required Field'),
    confirmPassword: yup.string().required().oneOf([yup.ref('password')], 'Password mismatch')
})


export const productSchema = yup.object().shape({
    name: yup.string().required().min(20).max(60),
    description: yup.string().max(200),
    price: yup.number().required().min(0.01).max(99999999.99),
    discount: yup.number().integer().min(10).max(90).nullable(),
    discountEnd: yup.date().min(new Date(), 'Discount end Date must be later than today'),
})