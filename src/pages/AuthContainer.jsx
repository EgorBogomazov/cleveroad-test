import React from "react";
import SignInForm from "../components/form/SignInForm";
import SignUpForm from "../components/form/SignUpForm";
import {Route, Switch, Redirect} from 'react-router-dom';

import './page.scss'

const AuthContainer = () => {
    return (
        <div className='auth-container'>
            <Switch>
                <Route path="/sign-up" exact component={SignUpForm}/>
                <Route path="/sign-in" exact component={SignInForm}/>
                <Redirect to="/sign-in"/>
            </Switch>
        </div>)
}


export default AuthContainer;