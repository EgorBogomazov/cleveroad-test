import React, {useState, useEffect} from "react";
import clsx from 'clsx';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ViewListIcon from '@material-ui/icons/ViewList';
import ShopIcon from '@material-ui/icons/Shop';
import AddIcon from '@material-ui/icons/Add';
import ExitToApp from "@material-ui/icons/ExitToApp";
import Products from "./Products/Products";
import Product from "./Product/Product"
import firebase from "../firebase/Firebase";

import './page.scss'
import {Switch, Route, Redirect, Link, NavLink} from "react-router-dom";
import {bindActionCreators} from "redux";
import {getProducts} from "../actions/productsActions";
import {signOut} from "../actions/userActions";
import {connect} from "react-redux";
import {PRODUCTS_COLLECTION} from "../firebase/collections";


const drawerWidth = 240;


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        width: '100%',
        height: '100%'
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
}));

const tabs = [
    {
        text: 'All',
        icon: () => (<ViewListIcon color='primary'/>),
        href: '/'
    },
    {
        text: 'Add Product',
        icon: () => (<AddIcon color='primary'/>),
        href: '/product'
    }
]


const MainContainer = (props) => {

    const {getProducts, signOut} = props;

    useEffect(()=>{
        getProducts()

        const unsubscribe = firebase.db.collection(PRODUCTS_COLLECTION)
            .onSnapshot((querySnapshot) => {
                getProducts();
            }, e => console.log(e));

        return () => unsubscribe();
    }, [])


    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <div className='main-container splash'>
            <div className={classes.root}>
                <CssBaseline/>
                <AppBar
                    position="fixed"
                    className={clsx(classes.appBar, {
                        [classes.appBarShift]: open,
                    })}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            className={clsx(classes.menuButton, open && classes.hide)}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" noWrap>
                            Custom Store
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer
                    className={classes.drawer}
                    variant="persistent"
                    anchor="left"
                    open={open}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                        </IconButton>
                    </div>
                    <Divider/>
                    <List>
                        {tabs.map((item) => (
                            <Link to={item.href}>
                                <ListItem button key={item.text}>
                                    <ListItemIcon>{item.icon()}</ListItemIcon>
                                    <ListItemText primary={item.text}/>
                                </ListItem>
                            </Link>
                        ))}
                    </List>
                    <Divider/>
                    <List>
                        <ListItem button onClick={signOut}>
                            <ListItemIcon><ExitToApp htmlColor='red'/></ListItemIcon>
                            <ListItemText primary='Sign Out'/>
                        </ListItem>
                    </List>
                </Drawer>
                <main
                    className={clsx(classes.content, {
                        [classes.contentShift]: open,
                    })}
                >
                    <Switch>
                        <Route exact path='/' component={Products}/>
                        <Route exact path='/product' component={Product}/>
                        <Route exact path='/product/:id' component={Product}/>
                        <Redirect to='/' />
                    </Switch>
                </main>
            </div>
        </div>
    )
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        products: state.products.collection,
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts, signOut
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);