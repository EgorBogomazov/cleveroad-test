import React from 'react';
import ProductDetails from '../../components/form/ProductDetails'

import "../page.scss"
import {useParams} from "react-router";


const Product = (props) => {

    const params = useParams();

    return (
        <div className='page splash'>
            <ProductDetails id={params.id}/>
        </div>
    )
}


export default Product;