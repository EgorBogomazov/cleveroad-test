import React from 'react';

import "../page.scss"
import ProductsList from "../../components/list/ProductsList";


const Products = (props) => {
    return (
        <div className='page'>
            <ProductsList/>
        </div>
    )
}


export default Products;