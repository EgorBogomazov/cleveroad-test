import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import './list.scss'
import {bindActionCreators} from "redux";
import {getProducts} from "../../actions/productsActions";
import ProductCard from "../card/ProductCard";
import LoadingLine from "../loading/LoadingLine";


const ProductsList = (props) => {

    const {products, loading} = props;



    return (
        loading ? <LoadingLine/> :
        <div className='list-container splash'>
            {products.map(product => <ProductCard key={product.id} product={product}/>)}
        </div>
    )
}


const mapStateToProps = (state) => {
    return {
        user: state.user,
        products: state.products.collection,
        loading: state.loading.products,
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductsList);