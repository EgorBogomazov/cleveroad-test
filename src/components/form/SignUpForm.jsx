import React, {useState} from "react";
import './form.scss'
import FormTitle from "../title/FormTitle";
import {Form} from 'react-final-form'
import {signUpSchema} from "../../utils/schema";
import {setIn} from 'final-form'
import FormTextField from "../input/FormTextField";
import {useHistory} from "react-router";
import {bindActionCreators} from "redux";
import {signUp} from "../../actions/userActions";
import {connect} from "react-redux";
import ContainedButton from "../button/ContainedButton";
import LoadingLine from "../loading/LoadingLine";

const initialValues = {email: '', password: '', name: '', confirmPassword: ''}


const SignUpForm = (props) => {

    const {signUp, loading} = props;
    const [formValue, setFormValue] = useState(null);

    const onSubmit = async values => {
        setFormValue({...values})
        signUp(values)
    };

    const history = useHistory();


    const validateFormValues = (schema) => async (values) => {
        if (typeof schema === 'function') {
            schema = schema();
        }
        try {
            await schema.validate(values, {abortEarly: false});
        } catch (err) {
            return err.inner.reduce((formError, innerError) => {
                return setIn(formError, innerError.path, innerError.message);
            }, {});
        }
    };

    const validate = validateFormValues(signUpSchema);

    return (
        <div className='sign-up-form splash'>
            {loading ? <LoadingLine/> :
            <>
                <FormTitle text='Registration'/>
                <Form
                    initialValues={formValue ? formValue : initialValues}
                    validate={validate}
                    onSubmit={onSubmit}
                    render={({handleSubmit, form, submitting, pristine, values}) => (
                        <div className='form-container'>
                            <FormTextField name='name' placeholder='Name'/>
                            <FormTextField name='email' placeholder='Email'/>
                            <FormTextField name='password' placeholder='Password'/>
                            <FormTextField name='confirmPassword' placeholder='Confirm Password'/>
                            <div className='d-flex space-around'>
                                <ContainedButton onClick={() => history.push('/sign-in')} type="button" text='Sign In'/>
                                <ContainedButton onClick={handleSubmit} text='Submit'
                                                 disabled={submitting || pristine}/>
                            </div>
                        </div>
                    )}

                />
            </>
            }

        </div>)
}


const mapDispatchToProps = dispatch => bindActionCreators({
    signUp
}, dispatch);

const mapStateToProps = (state) => {
    return {
        loading: state.loading.auth,
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);