import React, {useState} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux'
import {useHistory} from "react-router";
import {signIn} from '../../actions/userActions'
import {setIn} from 'final-form'
import FormTitle from "../title/FormTitle";
import FormTextField from "../input/FormTextField";
import {Form} from 'react-final-form'
import {signInSchema} from "../../utils/schema";
import './form.scss';
import ContainedButton from "../button/ContainedButton";
import LoadingLine from "../loading/LoadingLine";

const initialValues = {email: '', password: ''}




const SignInForm = (props) => {

    const {signIn, loading} = props;
    const [formValue, setFormValue] = useState(null);



    const onSubmit = async values => {
        setFormValue({...values})
        signIn(values)
    };

    const history = useHistory();


    const validateFormValues = (schema) => async (values) => {
        if (typeof schema === 'function') {
            schema = schema();
        }
        try {
            await schema.validate(values, {abortEarly: false});
        } catch (err) {
            return err.inner.reduce((formError, innerError) => {
                return setIn(formError, innerError.path, innerError.message);
            }, {});
        }
    };

    const validate = validateFormValues(signInSchema);

    return (
        <div className='sign-in-form splash'>
            {loading ? <LoadingLine/>
                :
                <>
                    <FormTitle text='Sign In'/>
                    <Form
                        initialValues={formValue ? formValue : initialValues}
                        validate={validate}
                        onSubmit={onSubmit}
                        render={({handleSubmit, form, submitting, pristine, values}) => (

                            <div className='form-container'>
                                <FormTextField name='email' placeholder='Email'/>
                                <FormTextField name='password' placeholder='Password'/>
                                <div className='d-flex space-around'>
                                    <ContainedButton onClick={() => history.push('/sign-up')} type="button"
                                                     text='Registration'/>
                                    <ContainedButton onClick={handleSubmit} type='submit' text='Submit'
                                                     disabled={submitting || pristine}/>
                                </div>
                            </div>
                        )}

                    />
                </>
            }

        </div>)
}


const mapDispatchToProps = dispatch => bindActionCreators({
    signIn
}, dispatch);

const mapStateToProps = (state) => {
    return {
        loading: state.loading.auth,
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(SignInForm);