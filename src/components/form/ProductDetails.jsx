import React, {useCallback, useEffect, useState} from "react";
import {connect} from 'react-redux'
import ProductView from '../card/ProductView';
import ProductEdit from "../card/ProductEdit";

import "./form.scss"
import firebase from "../../firebase/Firebase";
import {PRODUCTS_COLLECTION} from "../../firebase/collections";
import {bindActionCreators} from "redux";
import {getProducts} from "../../actions/productsActions";

const ProductDetails = (props) => {
    const {id, user, products, getProducts} = props;
    const [isEdit, setIsEdit] = useState(false);

    const findProduct = useCallback(products.find(item => item.id === id), [id])

    const product = id && findProduct ? findProduct : null
    const isAddNew = !id;

    useEffect(()=>{
        const unsubscribe = firebase.db.collection(PRODUCTS_COLLECTION).doc(id)
            .onSnapshot((querySnapshot) => {
                getProducts();
            }, e => console.log(e));

        return () => unsubscribe();
    }, [product])



    const cancelEdit = () => setIsEdit(false);
    const startEdit = () => {
        setIsEdit(true)
    };


    return (
        <div className='product'>
            {isEdit || isAddNew ? <ProductEdit isAddNew={isAddNew} cancelEdit={cancelEdit} product={product}/> :
                <ProductView isEditable={user.id === product.user} startEdit={startEdit} product={product}/>}
        </div>
    )

}

const mapStateToProps = (state) => {
    return {
        user: state.user.main,
        products: state.products.collection
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);