import React from "react";

import {TextField} from "@material-ui/core";
import {Field} from "react-final-form";


const FormTextField = ({name, placeholder}) => (
    <Field
        name={name}
    >
        {(props) =>
            <div className='d-flex direction-column'>
                <TextField
                    {...props.input}
                    placeholder={placeholder}
                    label={placeholder}
                    type={name}
                    fullWidth
                    variant='filled'
                    error={props.meta.touched ? props.meta.error : ''}
                />
                <div className='error-container'>
                    <div className='error-text'>{props.meta.touched ? props.meta.error : ''}</div>
                </div>
            </div>
        }
    </Field>
)


export default FormTextField
