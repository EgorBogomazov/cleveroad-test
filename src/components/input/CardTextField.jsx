import React from "react";

import {TextField} from "@material-ui/core";
import {Field} from "react-final-form";

import '../form/form.scss'


const CardTextField = ({name, placeholder}) => (
    <Field
        name={name}
    >
        {(props) =>
            <div className='d-flex direction-column w-100'>
                <TextField
                    {...props.input}
                    multiline
                    placeholder={placeholder}
                    rows={5}
                    label={placeholder}
                    type={name}
                    fullWidth
                    variant='filled'
                    error={props.meta.touched ? props.meta.error : ''}
                />
                <div className='error-container'>
                    <div className='error-text'>{props.meta.touched ? props.meta.error : ''}</div>
                </div>
            </div>
        }
    </Field>
)


export default CardTextField;
