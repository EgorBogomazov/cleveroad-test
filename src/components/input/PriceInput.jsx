import React from "react";

import {TextField} from "@material-ui/core";
import {Field} from "react-final-form";


const PriceInput = ({name, placeholder, startIcon}) => (
    <Field
        name={name}
    >
        {(props) =>
            <div className='d-flex direction-column w-100'>
                <TextField
                    {...props.input}
                    placeholder={placeholder}
                    label={placeholder}
                    type={'number'}
                    fullWidth
                    InputProps={{
                        startAdornment: startIcon,
                    }}
                    variant='filled'
                    error={props.meta.touched ? props.meta.error : ''}
                />
                <div className='error-container'>
                    <div className='error-text'>{props.meta.touched ? props.meta.error : ''}</div>
                </div>
            </div>
        }
    </Field>
)


export default PriceInput;
