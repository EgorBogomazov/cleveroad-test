import React from "react";

import {TextField, Input, InputLabel} from "@material-ui/core";
import {Field} from "react-final-form";


const DateInput = ({name, placeholder, startIcon}) => (
    <Field
        name={name}
    >
        {(props) =>
            <div className='d-flex direction-column w-100'>
                <InputLabel>{placeholder}</InputLabel>
                <Input
                    {...props.input}
                    label={placeholder}
                    type={'date'}
                    initialValue={props.input.value}
                    fullWidth
                    inputLabelProps={{focused: true}}
                    error={props.meta.touched ? props.meta.error : ''}
                />
                <div className='error-container'>
                    <div className='error-text'>{props.meta.touched ? props.meta.error : ''}</div>
                </div>
            </div>
        }
    </Field>
)


export default DateInput;
