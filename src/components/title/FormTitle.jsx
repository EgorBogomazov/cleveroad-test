import React from "react";
import './title.scss'

const FormTitle = (props) => {
    const {text} = props;
    return (
        <div className='form-title'>
            {text}
        </div> )
}


export default FormTitle;