import React from 'react'
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core/styles';
import './loading.scss'


const useStyles = makeStyles((theme) => ({
    root: {
        width: '80%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

const LoadingLine = (props) => {
    const classes = useStyles();
    const {color} = props;

    return (
        <div className={classes.root}>
            <LinearProgress color={color ? color : 'primary'}/>
        </div>
    )
}

export default LoadingLine;