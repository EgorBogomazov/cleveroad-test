import React from "react";
import {Button} from "@material-ui/core";
import {makeStyles} from '@material-ui/core/styles';

const useStyles = (makeStyles({
    button: {
        float: 'center',
        color: '#ffffff',
        borderRadius: 30,
        width: 190,
        fontSize: 14,
        textAlign: 'center',
        // '&:hover': {
        //     borderColor: tenant.mainColor,
        //     color: tenant.mainColor
        // },
        '&:disabled': {
            color: 'gray',
            backgroundColor: '#f9f9f9'
        }
    }
}));

const ContainedButton = (props) => {
    const classes = useStyles()

    const {text, onClick, type, disabled} = props;
    return (
        <Button variant="contained"
                onClick={onClick}
                className={classes.button}
                type={type}
                disabled={disabled}
                color='primary'>
            {text}
        </Button>
    )
}


export default ContainedButton;