import React, {useState} from "react";
import {useHistory} from "react-router";

import {makeStyles} from "@material-ui/core/styles";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Chip from '@material-ui/core/Chip';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import VisibilityIcon from '@material-ui/icons/Visibility';

import "./card.scss"
import IconButton from "@material-ui/core/IconButton";
import moment from "moment";


const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 95,
        width: 95,
        borderRadius: 15
    },
    head: {
        flex: 1,
        width: '100%'
    },
    priceContent: {
        flex: 1,
        width: '100%',
        paddingTop: 0,
        paddingRight: 0,
        display: "flex",
        flexDirection: 'column',
        alignItems: 'flex-end',

    },
    details: {
        flex: 1,
        width: '50%',
        paddingTop: 0,
        paddingBottom: 0,
    },
    content: {
        flex: 1,
        display: "flex",
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: '100%',
        padding: 0,
    },
    name: {
        display: 'inline-block',
        maxWidth: 200,
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    price: {
        width: 300,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'transparent'
    },
    description: {
        width: '100%',
        flex: 1,
        maxWidth: 500,
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    },
    label: {
        color: '#3eb396',
        fontSize: 24,
    },
    icon: {
        color: '#3eb396'
    },
    chip: {
        width: 200
    },
    discount: {
        background: 'white',
        width: 200,
        marginTop: 10,
        borderRadius: 13,
    },
    actions: {
        width: '100%',
        display: 'flex',
        alignItems:'flex-end',
        justifyContent: 'flex-end'
    }
});


const ProductCard = (props) => {
    const {product} = props;

    const classes = useStyles();
    const history = useHistory();

    const today = moment();

    const getDiscountDaysLeft = () => {
        if (product.discount && product.discountEnd) {
            const daysDiff = moment(product.discountEnd.toDate()).diff(today, 'days');
            if (daysDiff <= 0) return 'Expired'
            else return daysDiff
        } else {
            return 'N/A'
        }
    }

    const getTotalPrice = () => {
        const price = +product.price;
        const discount = +product.discount;
        if (discount && price && typeof getDiscountDaysLeft() === 'number' && getDiscountDaysLeft() > 0) {
            return (price - (price * discount / 100)).toFixed(2)
        } else {
            return price.toFixed(2)
        }
    }

    return (
        <div className='product-view'>
            <CardContent className={classes.head}>
                <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    height="140"
                    width="150"
                    className={classes.media}
                    image={product.fileUrl}
                    title={product.name}
                />

            </CardContent>

            <CardContent className={classes.content}>
                <CardContent className={classes.details}>
                    <Typography className={classes.name} gutterBottom color="primary" variant="h5" component="h2">
                        {product.name}
                    </Typography>
                    <Typography className={classes.description} variant="body2" color="primary" component="p">
                        {product.description}
                    </Typography>
                </CardContent>
                    <Chip
                        classes={{root: classes.chip, label: classes.label, icon: classes.icon}}
                        icon={<AttachMoneyIcon color='primary'/>}
                        label={getTotalPrice()}
                        size='medium'
                        color='secondary'
                    />
                    <ListItem className={classes.discount}>
                        <ListItemAvatar>
                            <Avatar>
                                <MoneyOffIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText primary={`Discount: ${product.discount ? product.discount : 'N/A'}%`}
                                      secondary={`Discount days left: ${getDiscountDaysLeft()}`}/>
                    </ListItem>

            </CardContent>
            <CardActions className={classes.actions}>
                <IconButton onClick={()=>{history.push(`product/${product.id}`)}} size="small" color="secondary">
                    <VisibilityIcon color='primary'/>
                </IconButton>
            </CardActions>
        </div>
    )
}

export default ProductCard;