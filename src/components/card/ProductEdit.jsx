import React, {useState, useEffect} from "react";
import {connect} from 'react-redux'
import {makeStyles} from "@material-ui/core/styles";
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import {Form} from 'react-final-form'
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import "./card.scss"
import IconButton from "@material-ui/core/IconButton";
import FormTextField from "../input/FormTextField";
import CardTextField from "../input/CardTextField";
import PriceInput from "../input/PriceInput";
import DateInput from "../input/DateInput";
import {productSchema} from "../../utils/schema";
import {setIn} from "final-form";
import {bindActionCreators} from "redux";
import {showImageAlert} from "../../actions/alertActions";
import {createProduct, updateProduct} from "../../actions/productsActions";
import LoadingLine from "../loading/LoadingLine";

const initialValues = {
    name: '',
    description: '',
    price: 0,
    discount: null,
    discountEnd: new Date(),
}


const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 300,
        width: '100%',
        borderRadius: 15
    },
    head: {
        flex: 1,
        width: '100%',
        padding: 10,
    },
    priceContent: {
        flex: 1,
        width: '100%',
        height: '100%',
        paddingTop: 0,
        paddingRight: 0,
        display: "flex",
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'space-between'

    },
    details: {
        flex: 1,
        width: '50%',
        height: '80%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingTop: 0,
        paddingBottom: 0,
    },
    content: {
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        padding: 0,
    },
    name: {
        width: '100%'
    },
    price: {
        width: 300,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'transparent'
    },
    description: {
        width: '100%',
        flex: 1
    },
    label: {
        color: '#3eb396',
        fontSize: 24,
    },
    icon: {
        color: '#3eb396'
    },
    chip: {
        width: 200
    },
    discount: {
        background: 'white',
        width: 200,
        marginTop: 10,
        borderRadius: 13,
    },
    actions: {
        width: '100%',
        display: 'flex',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    }
});


const ProductView = (props) => {

    const classes = useStyles();
    const {showImageAlert, user, createProduct, loading, cancelEdit, isAddNew, product, updateProduct} = props;
    const [formValue, setFormValue] = useState(null);
    const [file, setFile] = useState(null);
    const [fileUrl, setFileUrl] = useState(null)

    const _initialValues = product ? {
        name: product.name,
        description: product.description,
        price: product.price,
        discount: product.discount ? product.discount : null,
        discountEnd: new Date(product.discountEnd.toDate())
    } : initialValues


    useEffect(() => {
        if (product && product.fileUrl) setFileUrl(product.fileUrl)

    }, [product])

    const onSubmit = async values => {
        if (product) {
            updateProduct({...product, ...values, file})
            setFormValue({...values})
        } else {
            if (!file) {
                showImageAlert()
                return
            }
            setFormValue({...values})
            createProduct({...values, user: user.id, file})
        }

    };

    const onLoadFile = (event) => {
        if (event.target.files && event.target.files[0]) {
            const file = event.target.files[0]
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function (e) {
                const image = new Image();
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {
                    const height = this.height;
                    const width = this.width;
                    if (height < 200 || width < 200 || height > 4000 || width > 4000) {
                        showImageAlert()
                        return false;
                    }
                    setFile(file)
                    setFileUrl(image.src)
                    return true;
                };
            };
        }
    }


    const validateFormValues = (schema) => async (values) => {
        if (typeof schema === 'function') {
            schema = schema();
        }
        try {
            await schema.validate(values, {abortEarly: false});
        } catch (err) {
            return err.inner.reduce((formError, innerError) => {
                return setIn(formError, innerError.path, innerError.message);
            }, {});
        }
    };

    const validate = validateFormValues(productSchema);

    return (
        loading ? <LoadingLine/>
            : <Form initialValues={formValue ? formValue : _initialValues}
                    validate={validate}
                    onSubmit={onSubmit}
                    render={({handleSubmit, form, submitting, pristine, values}) => (
                        <div className='view'>
                            <CardActionArea className={classes.head}>
                                <input
                                    accept="image/*"
                                    className={classes.input}
                                    style={{display: 'none'}}
                                    id="raised-button-file"
                                    multiple
                                    onChange={onLoadFile}
                                    type="file"
                                />
                                <label htmlFor="raised-button-file">
                                    <CardMedia
                                        component="img"
                                        alt="Contemplative Reptile"
                                        height="140"
                                        width="150"
                                        className={classes.media}
                                        image={fileUrl ? fileUrl : "https://i.stack.imgur.com/y9DpT.jpg"}
                                        title="Upload Image"
                                    />
                                </label>

                            </CardActionArea>
                            <CardContent className={classes.content}>
                                <CardContent className={classes.details}>
                                    <FormTextField name='name' placeholder='Name'/>
                                    <CardTextField name='description' placeholder='Description'/>
                                </CardContent>
                                <CardContent className={classes.priceContent}>
                                    <PriceInput
                                        name='price' placeholder='Price'
                                    />
                                    <PriceInput name='discount' placeholder='Discount'/>
                                    <DateInput name='discountEnd' placeholder='Discount End Date'/>
                                </CardContent>
                            </CardContent>
                            <CardActions className={classes.actions}>
                                <IconButton onClick={handleSubmit} size="small" color="secondary">
                                    <SaveIcon color='primary'/>
                                </IconButton>
                                {!isAddNew ?
                                    <IconButton onClick={cancelEdit} size="small" color="secondary">
                                        <CancelIcon htmlColor='red'/>
                                    </IconButton> : null
                                }

                            </CardActions>
                        </div>
                    )}/>

    )
}

const mapStateToProps = (state) => {
    return {
        user: state.user.main,
        loading: state.loading.products
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showImageAlert, createProduct, updateProduct
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductView);