import React, {useState} from "react";

import {makeStyles} from "@material-ui/core/styles";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import EditIcon from '@material-ui/icons/Edit';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import Chip from '@material-ui/core/Chip';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import moment from 'moment'
import IconButton from "@material-ui/core/IconButton";
import {bindActionCreators} from "redux";
import {deleteProduct} from "../../actions/productsActions";
import {connect} from "react-redux";

import "./card.scss"
import {useHistory} from "react-router";


const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 400,
        width: '100%',
        borderRadius: 15
    },
    head: {
        flex: 1,
        width: '100%'
    },
    priceContent: {
        flex: 1,
        width: '100%',
        paddingTop: 0,
        paddingRight: 0,
        display: "flex",
        flexDirection: 'column',
        alignItems: 'flex-end',

    },
    details: {
        flex: 1,
        width: '50%',
        paddingTop: 0,
        paddingBottom: 0,
    },
    content: {
        flex: 1,
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        padding: 0,
    },
    name: {
        width: '100%'
    },
    price: {
        width: 300,
        height: 50,
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'transparent'
    },
    description: {
        width: '100%',
        flex: 1
    },
    label: {
        color: '#3eb396',
        fontSize: 24,
    },
    icon: {
        color: '#3eb396'
    },
    chip: {
        width: 200
    },
    discount: {
        background: 'white',
        width: 200,
        marginTop: 10,
        borderRadius: 13,
    },
    actions: {
        width: '100%',
        flex: 1,
        display: 'flex',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    }
});


const ProductView = (props) => {

    const {startEdit, isEditable, product, deleteProduct} = props;

    const classes = useStyles();
    const history = useHistory()
    const today = moment();

    const getDiscountDaysLeft = () => {
        if (product.discount && product.discountEnd) {
            const daysDiff = moment(product.discountEnd.toDate()).diff(today, 'days');
            console.log(daysDiff);
            if (daysDiff <= 0) return 'Expired'
            else return daysDiff
        } else {
            return 'N/A'
        }
    }

    const getTotalPrice = () => {
        const price = +product.price;
        const discount = +product.discount;

        if (discount && price && typeof getDiscountDaysLeft() === 'number' && getDiscountDaysLeft() > 0) {
            return (price - (price * discount / 100)).toFixed(2)
        } else {
            return price.toFixed(2)
        }
    }

    const onDeleteProduct = () => {
        deleteProduct(product.id)
        history.push('/')
    }


    return (
        <div className='view'>
            <CardContent className={classes.head}>
                <CardMedia
                    component="img"
                    alt="Contemplative Reptile"
                    className={classes.media}
                    image={product ? product.fileUrl : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAclBMVEX///8AAABgYGBpaWmtra2hoaH7+/vMzMzm5uY6OjrFxcXS0tJMTEzt7e3q6uri4uIQEBBYWFjz8/O9vb1TU1MaGhp7e3u3t7eUlJSNjY2np6fc3NyGhoYzMzM/Pz+bm5tycnImJiYoKCg1NTUVFRWIiIifPXEAAAAE6ElEQVR4nO2b61bqMBBGHS7ljshVRBQU3/8VjyK5NPna9ChJXKxv/7M1WbNpkkmGcndHCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQsgP2Mpz7hDiMl/JKHcMcVnIS5E7hri05DV3CHGZiNznjiEur9LKHUJkVrLIHUJcRnKc544hLgdZ5w4hLoXcejLsyEvuECIzlqfcIWB2ozO/70fkj64zLTlTuRkpFIF+tigZ7l7GZ6a/DPJXtL8Nq1aJzuV2KJkXexn6V9eXxt3fx/lzlOEMj7GmhkMR/+L8eGk8zrkhb9crNDVsy6N/caEay+4Kkf4UbYinYkPD6Uz6/tWW7jvnwdgYSg/cbmj4JEv/Yt90vco4TC3D48S/3cywGEvHv/pqukbLUCosQ3nwbzcz3MGFam913b5WvP+PbQhWi2aGH3LwL47sniXfMC0Z+lmxkWFxQul0XeoZjOJElA29nVcjQ1iBKsodD64a9f/gGLpLYiNDWIHqlDvOV8JxDN2p2MRwIigZvtR3nA7X0FnXmxh20SJ87/Z7vHLgjfEMV6WDQBNDWIHaqnYD/NGlwzMsP5AGhiNZgWR4vLTrdFUPhytH3hRtuJ6hGdPAcI0qUEPVbjrVgyPTCVkbdsxJwBpPYUNcgVKb7r215GxixB9GG3atFG02qGHDzZeFS18NiO3XQoSGfzosw0J/2qZqFjaEFagn1WxkHzHypETL0Frht+p20BBXoMaXVsevvc5S9ZHnmynb8HPEuVMxaPgIK1Cq1XkN0g80z7eLJUMzFU+XrBgyLN5RnnsufVBmmGYpipcN797Vn+Pv2yHDnsz8i/M31ep7yXpQf2YpZjiG5vP+noohw7aZsoah00h3MssxTB1Dqz62KAWHDadwgTQ5Vv+X3WdiXEMzh2ZfB4aAIaxAGSFlr4tuOYoZnmGhVvrzVKw3xBWort3BGbNGZ9i5eYZWVnwOGe5kBupzugKlN7hTvefN8P2Ub2h94ouAYaACZSrdepiCHV5sgKGZijKvNSxOqIqsc+rbff/CVCf9DDs3ZHinT63jWsOhDMDyL7WA5BIZaDjRAencjQxbqPiykVrABiEy0NDkbA0wnMMxN/aalklezMCGn2tI2PAVnfj6bkOX5CmxwrBwHwUwfEOn9seQIcovUakwNKf0SsMR3GbuXSOP1MWMKkNrg1phCCtQvaAg2udFpdLQmYqeYTFDxz1TnHzqlNiYL4QTp8Rqw/Kq6BluUBV7cqp8Umark7i+X2M4qTWEFSjzdYx/TtKpNXExo8awlBVdQ1yB0kUnsAiZVTZtMaPO0J6KruEjSmy6AoU2Z+bMkvYtzVpDayo6hsUAbU62+t/RCzT6Aaet79cbmoOdY9hDL5AUeqrBjGAOGEmLGfWGXlFJcZCPmn/G39qb6kbSN94Dhnp9KAc1h+NQ93XCOzOTElPu3Nb7wZm3qpcllsiwg8ahyS4VS4l5xn/qlyfT1sMny3Ixd4nG4WTYu1DxiAp1v/fn3wi/T39CSMxWDrlDiMwMvsd4Qwzl/eZ/hZft9Z804ArULdFNfkxPzT7XeyOpGOV8HTYJsAJ1S9z+T9I3ssodQmQ+Hm58nSGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQggh1+UfFKcouk0afC0AAAAASUVORK5CYII='}
                    title="Contemplative Reptile"
                />

            </CardContent>

            <CardContent className={classes.content}>
                <CardContent className={classes.details}>
                    <Typography className={classes.name} gutterBottom color="primary" variant="h5" component="h2">
                        {product.name}
                    </Typography>
                    <Typography className={classes.description} variant="body2" color="primary" component="p">
                        {product.description}
                    </Typography>
                </CardContent>
                <CardContent className={classes.priceContent}>
                    <Chip
                        classes={{root: classes.chip, label: classes.label, icon: classes.icon}}
                        icon={<AttachMoneyIcon color='primary'/>}
                        label={getTotalPrice()}
                        size='medium'
                        color='secondary'
                    />
                    <ListItem className={classes.discount}>
                        <ListItemAvatar>
                            <Avatar>
                                <MoneyOffIcon/>
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            primary={`Discount: ${product.discount ? product.discount + '%' : 'N/A'}`}
                            secondary={`Discount days left: ${getDiscountDaysLeft()}`}/>
                    </ListItem>
                </CardContent>

            </CardContent>
            <CardActions className={classes.actions}>
                {isEditable ?
                    <IconButton onClick={startEdit} size="small" color="secondary">
                        <EditIcon color='primary'/>
                    </IconButton> : null
                }
                {isEditable && product ?
                    <IconButton onClick={onDeleteProduct} size="small" color="secondary">
                        <DeleteOutlineIcon htmlColor='red'/>
                    </IconButton> : null
                }

            </CardActions>
        </div>
    )
}

const mapDispatchToProps = dispatch => bindActionCreators({
    deleteProduct
}, dispatch);

export default connect(null, mapDispatchToProps)(ProductView);