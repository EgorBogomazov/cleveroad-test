import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

import firebaseConfig from '../config/firebaseConfig';

class Firebase {
    constructor() {
        app.initializeApp(firebaseConfig);

        this.auth = app.auth();
        this.db = app.firestore();
        this.storage = app.storage();
    }

    doSignInWithEmailAndPassword = (email, password) => {
        return this.auth.signInWithEmailAndPassword(email, password)
    };

    doSignUpWithEmailAndPassword = (email, password) => {
        return this.auth.createUserWithEmailAndPassword(email, password)
    }

    doGetCurrentUser = () => {
        return this.auth;
    };

    doSignOut = () => {
        return this.auth.signOut()
    };


}

const instance = new Firebase();

export default instance;
