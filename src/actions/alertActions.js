import {CLEAR_ALERT_STATE, SHOW_ALERT} from "./actionTypes";


export const hideAlert = () => dispatch => dispatch({type: CLEAR_ALERT_STATE})


export const showImageAlert = () => dispatch => dispatch({
    type: SHOW_ALERT,
    value: {open: true, variant: "error", message: "Image width/height - 200px-400px"}
})