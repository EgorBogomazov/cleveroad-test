import {SET_USER, SHOW_ALERT, START_AUTH_LOADING, STOP_AUTH_LOADING} from "./actionTypes";
import firebase from '../firebase/Firebase'
import {USERS_COLLECTION} from "../firebase/collections";


export const signUp = ({email, password, name}) => async dispatch => {
    dispatch({type: START_AUTH_LOADING})
    await firebase.doSignUpWithEmailAndPassword(email, password)
        .then(async res => {
            const id = res.user.uid
            await firebase.db.collection(USERS_COLLECTION).doc(id)
                .set({name, email})
                .catch(err => err)
            dispatch({type: SET_USER, user: {email, name, id}});
            dispatch({type: STOP_AUTH_LOADING});
            return null;
        }).catch(err => {
            dispatch({type: STOP_AUTH_LOADING});
            dispatch({type: SHOW_ALERT, value: {message: err.message, variant: 'error'}});
            return null;
        })
}

export const signIn = ({email, password}) => async dispatch => {
    dispatch({type: START_AUTH_LOADING})
    await firebase.doSignInWithEmailAndPassword(email, password)
        .then(async auth => {
            const userSnapshot = await firebase.db.doc(`${USERS_COLLECTION}/${auth.user.uid}`).get().catch(err => err);
            dispatch({type: SET_USER, user: {...userSnapshot.data(), id: userSnapshot.id}});
            dispatch({type: STOP_AUTH_LOADING});
            return null;
        }).catch(err => {
            dispatch({type: STOP_AUTH_LOADING});
            dispatch({type: SHOW_ALERT, value: {message: err.message, variant: 'error'}});
            return null;
        })
}


export const getCurrentUser = () => async dispatch => {
    dispatch({type: START_AUTH_LOADING})
    await firebase.doGetCurrentUser().onAuthStateChanged(async userSnapshot => {
        if(userSnapshot && userSnapshot.uid) {
            const user = await firebase.db.collection(USERS_COLLECTION)
                .doc(userSnapshot.uid)
                .get()
            dispatch({type: SET_USER, user: {...user.data(), id: user.id}})
            dispatch({type: STOP_AUTH_LOADING})
        } else {
            dispatch({type: SET_USER, user: {}})
            dispatch({type: STOP_AUTH_LOADING})
        }
    })
}

export const signOut = () => dispatch => {
    firebase.doSignOut().then(()=>{
        dispatch({
            type: SET_USER, user: {}
        })
    })
}