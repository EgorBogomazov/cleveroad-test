import {SET_PRODUCTS, START_PRODUCTS_LOADING, STOP_PRODUCTS_LOADING} from "./actionTypes";
import firebase from '../firebase/Firebase'
import {PRODUCTS_COLLECTION} from "../firebase/collections";

export const createProduct = (data) => async dispatch => {
    dispatch({type: START_PRODUCTS_LOADING})
    const documentRef = firebase.db.collection(PRODUCTS_COLLECTION).doc()
    const docId = documentRef.id;
    const storage = firebase.storage.ref();
    await storage.child(`${docId}/mainPhoto`).put(data.file)
    const document = {...data, discountEnd: new Date(data.discountEnd)}
    delete document.file
    await documentRef.set(document)
    dispatch({type: STOP_PRODUCTS_LOADING})
}

export const updateProduct = (data) => async dispatch => {
    dispatch({type: START_PRODUCTS_LOADING})
    if(data.file) {
        const storage = firebase.storage.ref();
        await storage.child(`${data.id}/mainPhoto`).put(data.file)
    }
    const {description, discount, discountEnd, name, price, user} = data;
    await firebase.db.collection(PRODUCTS_COLLECTION).doc(data.id).update({description, discount, discountEnd: new Date(discountEnd), name, price, user})
    dispatch({type: STOP_PRODUCTS_LOADING})
}

export const deleteProduct = (id) => async dispatch => {
    dispatch({type: START_PRODUCTS_LOADING})
    await firebase.db.collection(PRODUCTS_COLLECTION).doc(id).delete()
    dispatch({type: STOP_PRODUCTS_LOADING})
}


export const getProducts = () => async dispatch => {
    dispatch({type: START_PRODUCTS_LOADING})
    const storage = firebase.storage.ref();
    const productsSnapshot = await firebase.db.collection(PRODUCTS_COLLECTION).get();
    const products = productsSnapshot.docs.map(item => ({...item.data(), id: item.id}))
    const result = [...products]
    await Promise.all(products.map((product, index) => storage.child(`${product.id}/mainPhoto`)
        .getDownloadURL()
        .then((link) => {result[index].fileUrl = link;})))
    dispatch({type: SET_PRODUCTS, products: result})
    dispatch({type: STOP_PRODUCTS_LOADING})
}