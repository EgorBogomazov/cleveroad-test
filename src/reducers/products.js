import {SET_PRODUCTS, CREATE_PRODUCT} from "../actions/actionTypes";


const initialState = {
    isLoaded: false,
    collection: []
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_PRODUCTS:
            return {collection: action.products, isLoaded: true};
        default:
            return state;
    }
};

export default productsReducer;