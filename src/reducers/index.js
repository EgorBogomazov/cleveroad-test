import products from './products';
import user from './user';
import loading from './loading';
import alert from './alert'

export default {
    user,
    products,
    loading,
    alert
}