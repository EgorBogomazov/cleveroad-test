import {SET_USER} from "../actions/actionTypes";


const initialState = {
    isLoaded: false,
    main: {}
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            return {main: action.user, isLoaded: true};
        default:
            return state;
    }
};

export default userReducer;