import {
    START_AUTH_LOADING,
    START_PRODUCTS_LOADING,
    STOP_AUTH_LOADING,
    STOP_PRODUCTS_LOADING
} from "../actions/actionTypes";


const initialState = {
    auth: false,
    items: false,
    products: false
};

const loadingReducer = (state = initialState, action) => {
    switch (action.type) {
        case START_AUTH_LOADING:
            return {...state, auth: true};
        case STOP_AUTH_LOADING:
            return {...state, auth: false};
        case START_PRODUCTS_LOADING: {
            return {...state, products: true}
        }
        case STOP_PRODUCTS_LOADING:
            return {...state, products: false};
        default:
            return state;
    }
};

export default loadingReducer;