import {CLEAR_ALERT_STATE, SHOW_ALERT} from "../actions/actionTypes";


const initialState = {
    open: false,
    variant: 'error',
    message: ''
};

const alertReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_ALERT:
            return {...state, open: true, variant: action.value.variant, message: action.value.message};
        case CLEAR_ALERT_STATE:
            return initialState;
        default:
            return state;
    }
};

export default alertReducer;