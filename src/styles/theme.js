import {createMuiTheme} from "@material-ui/core";


const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#3eb396",
            light: "#20c997",
            dark: "#343a40",
            contrastText: "#ffffff",
            danger: "#d32f2f"
        },
        secondary: {
            main: "#e9e9e9",
            light: "#f2f2f2",
            dark: "#333333",
            contrastText: "#ffffff",
            danger: "#d32f2f"
        },
        inherit: {
            main: '#5b667d',
            light: "#f2f2f2",
            dark: "#333333",
            contrastText: "#ffffff",
            danger: "#d32f2f"
        },
        danger: {
            main: "#d32f2f",
            light: "#f2f2f2",
            dark: "#333333",
            contrastText: "#ffffff",
            danger: "#d32f2f"
        }
    }
});

export default theme;