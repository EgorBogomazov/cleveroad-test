import './App.css';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import AuthContainer from "./pages/AuthContainer";
import Alert from "./components/alert/Alert";
import {hideAlert} from "./actions/alertActions";
import {getCurrentUser} from "./actions/userActions";
import MainContainer from "./pages/MainContainer";
import React, {useEffect} from "react";

function App(props) {
    const {alert, hideAlert, getCurrentUser, user} = props;

    useEffect(()=>{
        getCurrentUser()
    }, [])


    return (
        <div className="App">
            {user.isLoaded && user.main.id ? <MainContainer/> : <AuthContainer/>}
            <Alert onClose={hideAlert} horizontal='center' vertical='bottom' variant={alert.variant} message={alert.message} open={alert.open}/>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        alert: state.alert
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    hideAlert, getCurrentUser
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
